<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="container-fluid ur-main ur-stripped">
    <div class="row-fluid">
        <div class="span2">
            <div id="channelListFixedContainer" class="ur-sidebar-fixed span2">
                <div class="ur-lightbox">
                    <div class="ur-header"><h1>Channels</h1></div>
                    <div id="channelListScrollContainer" class="ur-sidebar-scroll ur-content">
                        <ul id="channelList" class="nav nav-list ur-nav-list ur-list-icon ur-list-icon-light">
                            <li><i class="icon-spinner icon-spin"></i> Loading channels...</li>
                        </ul>
                    </div>
                    <div class="ur-actions text-center">
                        <button id="addFeed" class="btn btn-success btn-small"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Add Feed</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="span2">
            <div id="feedListFixedContainer" class="ur-sidebar-fixed span2">
                <div class="ur-lightbox">
                    <div id="feedList">
                    </div>
                </div>
            </div>
        </div>

        <div id="feedEntries" class="span8">
            <div class="row-fluid">
                <div class="span12 ur-entry">
                    <h3 class="ur-title">Welcome to <span class="ur-brand">uno<span>.</span>reader</span></h3>
                    <div class="ur-intro">
                        <span class="ur-info-item">your cozy newsfeed reader</span>
                        <span class="ur-info-item">elegant and simple</span>
                        <span class="ur-info-item">access from anywhere</span>
                        <span class="ur-info-item">enjoy</span>
                    </div>
                    <div class="row-fluid">
                        <div class="span5">
                            <h4>Get started with these recommended newsfeed packs</h4>
                            <button data-pack="apple" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Apple News</button>
                            <button data-pack="art" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Arts & Photos</button>
                            <button data-pack="comedy" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Comedy</button>
                            <button data-pack="news-spanish" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Noticias en espa�ol</button>
                            <button data-pack="ebooks-kindle" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Kindle eBooks</button>
                            <button data-pack="software" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Software Development</button>
                            <button data-pack="sports" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Sports</button>
                            <button data-pack="technology" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Technology</button>
                            <button data-pack="videogames" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;Videogames</button>
                            <button data-pack="news-world" class="js-add-pack btn btn-info btn-block ur-btn-text-left"><i class="icon-plus-sign"></i>&nbsp;&nbsp;&nbsp;World News</button>
                        </div>
                        <sec:authorize access="isAuthenticated()">
                            <div class="span6 offset1 ur-news">
                                <h4>Recent Updates</h4>
                                <ul class="well icons-ul">
                                    <li class="nav-header">Sat, 27 Jul 2013</li>
                                    <li><i class="icon-li icon-ok"></i>Minor fixes in bulleted lists.</li>
                                    
                                    <li class="nav-header">Fri, 21 Jun 2013</li>
                                    <li><i class="icon-li icon-ok"></i>Performance improvements.</li>

                                    <li class="nav-header">Wed, 6 Jun 2013</li>
                                    <li><i class="icon-li icon-ok"></i>Recover forgotten password feature.</li>
                                    <li><i class="icon-li icon-ok"></i>Minor fix when opening links in new tabs with middle-click.</li>

                                    <li class="nav-header">Wed, 22 May 2013</li>
                                    <li><i class="icon-li icon-ok"></i>Performance improvements.</li>

                                    <li class="nav-header">Sun, 5 May 2013</li>
                                    <li><i class="icon-li icon-ok"></i>Right-click context menu for editing feeds and channels.</li>
                                    <li><i class="icon-li icon-ok"></i>Taphold support for touchscreens.</li>
                                    <li><i class="icon-li icon-ok"></i>Various visual improvements.</li>

                                    <li class="nav-header">Thu, 2 May 2013</li>
                                    <li><i class="icon-li icon-ok"></i>Scrollbar for channels and feeds panels.</li>
                                    <li><i class="icon-li icon-ok"></i>Minor fixes.</li>
                                </ul>
                            </div>
                        </sec:authorize>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Used for load-on-scroll -->
    <div class="row-fluid ur-entries-bottom"></div>

</div>


<sec:authorize access="!isAuthenticated()">
    <div class="ur-hero-bottom">
        <div class="container">
            <div class="row">
                <div class="span4">
                    <div class="ur-item">
                        <i class="icon-rss"></i>
                        <h3>Newsfeed reader</h3>
                        <p>An elegant, simple and fast newsfeed reader. Read the news from your favourite websites in one place.</p>
                    </div>
                </div>
                <div class="span4">
                    <div class="ur-item">
                        <i class="icon-cloud-upload"></i>
                        <h3>In the cloud</h3>
                        <p>Your channels and feeds are stored in the cloud, so you can access them from any device, everywhere.</p>
                    </div>
                </div>
                <div class="span4">
                    <div class="ur-item">
                        <i class="icon-beer"></i>
                        <h3>Free, as in Beer</h3>
                        <p>Free to use and enjoy. No restrictions, no strings attached, and an awesome experience.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</sec:authorize>


<!--********************************************************************
                               MODALS
*********************************************************************-->
<div id="editFeedModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addFeedModalLabel" aria-hidden="true">
</div>

<div id="editChannelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editChannelModalLabel" aria-hidden="true">
</div>

<div id="confirmDeleteChannelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteChannelModalLabel" aria-hidden="true">
</div>

<div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3>Sign In to customize your experience</h3>
    </div>
    <div class="modal-body">
        <p>Your personal free account will allow you to:</p>
        <ul class="icons-ul">
            <li><i class="icon-li icon-ok"></i>Add your own newsfeeds.</li>
            <li><i class="icon-li icon-ok"></i>Create news channels.</li>
            <li><i class="icon-li icon-ok"></i>Access hand-picked newsfeed packs.</li>
            <li><i class="icon-li icon-ok"></i>Save your favourite articles for reading later.</li>
            <li><i class="icon-li icon-ok"></i>Access Uno Reader from any computer, tablet or smartphone.</li>
        </ul>
    </div>
    <div class="modal-footer">
        <a class="btn btn-success" href="<c:url value="/signup.html"/>">Create Account</a>
        <a class="btn btn-success" href="<c:url value="/login.html"/>">Sign In</a>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>


<!--********************************************************************
                             TEMPLATES
*********************************************************************-->
<script id="channelListTemplate" type="text/x-jsrender">
    <li><i class="icon-chevron-right"></i><a href="#" class="js-channel">All channels</a></li>
    {{for channels}}
        <li class="{{:~classForChannelItem(#data)}}"><i class="icon-chevron-right"></i><a href="#" class="js-channel js-channel-item" data-channel="{{:#data}}">{{:#data}}</a></li>
    {{/for}}
    <li class="ur-separator"><i class="icon-chevron-right"></i><a href="#" class="js-favourites">Favourites</a></li>
</script>

<script id="feedListTemplate" type="text/x-jsrender">
    <div class="ur-header">
        <h1>
            {{if channel}}
                {{:channel}}
            {{else}}
                Feeds
            {{/if}}
        </h1>
    </div>
    <div id="feedListScrollContainer" class="ur-sidebar-scroll ur-content">
        <ul class="nav nav-list ur-list-icon">
            {{if feeds.length > 1}}
                <li><i class=""></i><a href="#" class="js-feed">All feeds</a></li>
            {{/if}}
            {{for feeds}}
                <li class="{{:~classForFeedItem(id)}}"><i class=""></i><a href="#" class="js-feed js-feed-item" data-feed-id="{{:id}}">{{:name}}</a></li>
            {{/for}}
        </ul>
    </div>
</script>

<script id="feedListEmptyTemplate" type="text/x-jsrender">
    <div class="ur-help">
        <i class="icon-info-sign"></i>
        <p>
            <strong>Select a channel to start.</strong>
            <hr/>
            Right-click on feeds or channels to edit them.
            <hr/>
            Drag feeds into channels to organize them.
            <hr/>
            On touch devices, tap and hold to edit.
        </p>
    </div>
</script>

<script id="feedEntriesTemplate" type="text/x-jsrender">
    <div class="row-fluid">
        <div class="span12 ur-entry">
            <h3 class="ur-title"><a target="_blank" href="{{:link}}">{{:title}}</a></h3>
            <div class="ur-intro">
                {{if publishedDate}}
                    <span class="ur-info-item">{{formatTimeAgo:publishedDate}}</span>
                    <span class="ur-info-item">{{formatDate:publishedDate}}</span>
                {{/if}}
                {{if author}}
                    <span class="ur-info-item">by {{:author}}</span>
                {{/if}}
                <span class="ur-info-item"><a href="#" class="js-favourite-entry" data-active="false" data-link="{{:link}}" data-title="{{>title}}" data-snippet="{{>contentSnippet}}"><i class="icon-star-empty"></i> Favourite</a></span>
                <span class="ur-info-item"><a href="#" class="js-share-twitter" data-link="{{:link}}"><i class="icon-twitter-sign"></i> Share</a></span>
                <span class="ur-info-item"><a href="#" class="js-share-facebook" data-link="{{:link}}"><i class="icon-facebook-sign"></i> Share</a></span>
            </div>
            <p>{{:content}}</p>
        </div>
    </div>
</script>

<script id="feedEntriesDateTemplate" type="text/x-jsrender">
    <div class="row-fluid">
        <div class="span12 ur-entry ur-entry-date {{:cssClass}}">
            <p>{{:date}}</p>
        </div>
    </div>
</script>

<script id="editFeedModalTemplate" type="text/x-jsrender">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3>{{:title}}</h3>
    </div>
    <form id="editFeedForm">
        <input type="hidden" name="id" />
        <div class="modal-body">
            <label>Feed URL</label>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-link"></i></span>
                <input type="url" class="span5" name="url" placeholder="Enter the feed URL..." required="required" />
            </div>
            <label>Name</label>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-tag"></i></span>
                <input type="text" class="input-xlarge" name="name" placeholder="Newsfeed name..." required="required" />
            </div>
            <label>Channel</label>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-th-large"></i></span>
                <input type="text" class="input-xlarge" name="channel" placeholder="Channel name..." required="required" />
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-success" value="{{:saveButtonText}}"/>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </form>
</script>

<script id="editChannelModalTemplate" type="text/x-jsrender">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3>Rename Channel</h3>
    </div>
    <form id="editChannelForm">
        <input type="hidden" class="input-xlarge" name="oldChannel" value="{{:channel}}" />
        <input type="hidden" name="id" />
        <div class="modal-body">
            <label>Channel Name</label>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-th-large"></i></span>
                <input type="text" class="input-xlarge" name="newChannel" placeholder="Channel name..." required="required" value="{{:channel}}"/>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-success" value="Update Channel"/>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </form>
</script>

<script id="confirmDeleteChannelModalTemplate" type="text/x-jsrender">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3>Are you sure you want to delete this channel?</h3>
    </div>
    <div class="modal-body">
        <p>You are about to delete the channel "{{:#data}}" and ALL its feeds.</p>
    </div>
    <div class="modal-footer">
        <button id="confirmDeleteChannelButton" data-channel="{{:#data}}" class="btn btn-danger"><i class="icon-trash"></i>&nbsp;&nbsp;&nbsp;Delete Channel</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
</script>

<script id="favouriteListTemplate" type="text/x-jsrender">
    <div class="row-fluid">
        <div class="span12 ur-entry">
            <h3 class="ur-title"><a target="_blank" href="{{:url}}">{{:title}}</a></h3>
            <div class="ur-intro">
                <span class="ur-info-item"><a href="#" class="js-favourite-remove" data-id="{{:id}}"><i class="icon-star"></i> Remove Favourite</a></span>
                <span class="ur-info-item"><a href="#" class="js-share-twitter" data-link="{{:url}}"><i class="icon-twitter-sign"></i> Share</a></span>
                <span class="ur-info-item"><a href="#" class="js-share-facebook" data-link="{{:url}}"><i class="icon-facebook-sign"></i> Share</a></span>
            </div>
            <p>{{:snippet}}</p>
        </div>
    </div>
</script>

<script id="favouriteListEmptyTemplate" type="text/x-jsrender">
    <div class="ur-help">
        <i class="icon-star"></i>
        <p>
            <strong>Click on the star to add or remove a Favourite.</strong>
            <hr/>
            You can always check your favourite articles from the Favourites Channel.
        </p>
    </div>
</script>

