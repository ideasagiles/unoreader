/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
unoreader.webreader = (function () {
    /** Loads a feed.
     * @param {Object} options The options to load the url. Valid attributes:
     *   url: the feed URL to load.
     *   includeHistorialEntries: true to fetch historial entries from the feed.
     *   numEntries: the maximum number of entries to fetch.
     *   success: the success callback function.
     *   error: the error callback function.
     */
    function loadFeed(options) {
        var feed = new google.feeds.Feed(options.url);
        if (options.includeHistorialEntries) feed.includeHistoricalEntries();
        if (options.numEntries) feed.setNumEntries(options.numEntries);
        feed.load(function(result) {
            if (!result.error) {
                options.success(result);
            }
            else {
                if (options.error) {
                    options.error(result);
                }
            }
        });
    }

    function findFeedMetadata(url, success, error) {
        loadFeed({
            url: url,
            includeHistorialFeeds: false,
            numEntries: 0,
            success: success,
            error: error
        });
    }

    /** Returns the date format for the dates in the feeds. */
    function getDateFormatPattern() {
        //expected format:   Wed, 03 Apr 2013 17:14:12 +0000
        return "ddd[,] DD MMMM YYYY HH:mm:ss Z";
    }

    return {
        findFeedMetadata: findFeedMetadata,
        loadFeed: loadFeed,
        getDateFormatPattern: getDateFormatPattern
    };

})();