/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.controller;

import com.ideasagiles.reader.service.ChannelService;
import com.ideasagiles.reader.vo.ChannelUpdateVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class ChannelController {

    @Autowired
    private ChannelService channelService;

    @RequestMapping(value = "/channels", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addPackToUser(@RequestBody ChannelUpdateVo channelUpdateVo) {
        channelService.rename(channelUpdateVo.getOldChannel(), channelUpdateVo.getNewChannel());
    }

    @RequestMapping(value = "/channels/{channel}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteChannelAndFeeds(@PathVariable String channel) {
        channelService.deleteChannelAndFeeds(channel);
    }
}
