/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.controller;

import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.service.UserService;
import com.ideasagiles.reader.vo.UserUpdateVo;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public @ResponseBody User save(@RequestBody User user, HttpServletRequest request) {
        String plainTextPassword = user.getPassword();
        userService.save(user, plainTextPassword);
        authenticateUserAndSetSession(user.getUsername(), plainTextPassword, request);
        return user;
    }

    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody UserUpdateVo user) {
        userService.updateLoggedInUser(user);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public @ResponseBody List<User> findAll() {
        return userService.findAll();
    }

    private void authenticateUserAndSetSession(String username, String plainTextPassword, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, plainTextPassword);

        // generate session if one doesn't exist
        request.getSession();

        token.setDetails(new WebAuthenticationDetails(request));
        Authentication authenticatedUser = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public @ResponseBody String passwordMismatch(Exception ex, HttpServletRequest request) {
        return "Current password does not match.";
    }
}
