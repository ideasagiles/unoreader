/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.repository;

import com.ideasagiles.reader.domain.Feed;
import com.ideasagiles.reader.domain.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface FeedRepository extends CrudRepository<Feed, Long> {

    List<Feed> findByUser(User user);
    List<Feed> findByChannelAndUser(String channel, User user);
}
