/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service.impl;

import com.ideasagiles.reader.domain.Authority;
import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.repository.AuthorityRepository;
import com.ideasagiles.reader.repository.UserRepository;
import com.ideasagiles.reader.service.UserService;
import com.ideasagiles.reader.vo.UserUpdateVo;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final String AUTHORITY_USER = "user";

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void save(User user, String plainTextPassword) {
        user.setPassword(passwordEncoder.encode(plainTextPassword));
        user.setMemberSince(new Date());
        user.setEnabled(true);
        userRepository.save(user);

        Authority authority = new Authority();
        authority.setUser(user);
        authority.setAuthority(AUTHORITY_USER);
        authorityRepository.save(authority);
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public void updateLoggedInUser(UserUpdateVo updateData) {
        User user = findLoggedInUser();
        if (updateData.getNewPassword() != null) {
            if (passwordEncoder.matches(updateData.getOldPassword(), user.getPassword())) {
                user.setPassword(passwordEncoder.encode(updateData.getNewPassword()));
            }
            else {
                throw new BadCredentialsException("Old password does not match.");
            }
        }
        user.setUsername(updateData.getUsername());
    }

    @PreAuthorize("isAuthenticated()")
    @Override
    public User findLoggedInUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.getByUsername(username);
    }

    @PreAuthorize("hasRole('admin')")
    @Override
    public List<User> findAll() {
        return userRepository.findAllOrderByMemberSince();
    }
}

