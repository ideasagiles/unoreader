/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

/**
 * Util methods for converting objects to different representations.
 */
public class ConversionTestUtils {

    public static String toJSON(Object o) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SerializationConfig annotationsDisabled = mapper.getSerializationConfig().without(SerializationConfig.Feature.USE_ANNOTATIONS);
        mapper.setSerializationConfig(annotationsDisabled);
        return mapper.writeValueAsString(o);
    }
}
