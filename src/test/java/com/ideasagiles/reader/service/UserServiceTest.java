/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.ideasagiles.reader.service;

import com.ideasagiles.reader.AbstractDatabaseTest;
import com.ideasagiles.reader.SecurityTestUtils;
import com.ideasagiles.reader.domain.User;
import com.ideasagiles.reader.repository.UserRepository;
import com.ideasagiles.reader.vo.UserUpdateVo;
import java.util.List;
import javax.validation.ConstraintViolationException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class UserServiceTest extends AbstractDatabaseTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void save_validUser_insertsUserInDatabase() {
        User user = new User();
        String plainTextPassword = "test";
        user.setUsername("test-user@ideasagiles.com");

        userService.save(user, plainTextPassword);

        assertNotNull(user.getId());
        User newUser = userRepository.findOne(user.getId());
        assertNotNull(newUser);
        assertEquals(user.getId(), newUser.getId());
        assertEquals(user.getUsername(), newUser.getUsername());
        assertNotEquals("Password must be hashed", plainTextPassword, user.getPassword());
        assertTrue(user.isEnabled());
        assertNotNull(user.getMemberSince());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void save_usernameExists_throwsException() {
        User user = new User();
        String plainTextPassword = "test";
        user.setUsername("leonardo.deseta@ideasagiles.com");

        userService.save(user, plainTextPassword);
    }

    @Test(expected = ConstraintViolationException.class)
    public void save_usernameIsNotAnEmail_throwsException() {
        User user = new User();
        String plainTextPassword = "test";
        user.setUsername("notAnEmail");

        userService.save(user, plainTextPassword);
    }

    @Test(expected = ConstraintViolationException.class)
    public void save_usernameIsBlank_throwsException() {
        User user = new User();
        String plainTextPassword = "test";
        user.setUsername("   ");

        userService.save(user, plainTextPassword);
    }

    @Test
    public void update_userChangesNothing_doesNothing() {
        SecurityTestUtils.loginTestUser();
        UserUpdateVo user = new UserUpdateVo();
        user.setUsername(SecurityTestUtils.USER_DEFAULT);
        userService.updateLoggedInUser(user);

        User modifiedUser = userService.findLoggedInUser();
        assertEquals(SecurityTestUtils.USER_DEFAULT, modifiedUser.getUsername());
    }

    @Test
    public void update_userChangesUsername_updatesUser() {
        SecurityTestUtils.loginTestUser();
        String newUsername = "other@ideasagiles.com";
        UserUpdateVo user = new UserUpdateVo();
        user.setUsername(newUsername);

        userService.updateLoggedInUser(user);
        userRepository.getByUsername(newUsername); //force Hibernate to commit changes

        SecurityTestUtils.login(newUsername, SecurityTestUtils.PASSWORD_DEFAULT);
        User modifiedUser = userService.findLoggedInUser();
        assertEquals(newUsername, modifiedUser.getUsername());
    }

    @Test(expected = BadCredentialsException.class)
    public void update_userChangesPasswordOldPasswordMismatch_throwsSecurityException() {
        SecurityTestUtils.loginTestUser();
        UserUpdateVo user = new UserUpdateVo();
        user.setUsername(SecurityTestUtils.USER_DEFAULT);
        user.setOldPassword("a wrong password");
        user.setNewPassword("a new password");

        userService.updateLoggedInUser(user);
    }

    @Test
    public void update_userChangesPasswordOldPasswordOk_updatesUser() {
        SecurityTestUtils.loginTestUser();
        String newPassword = "a new password";
        UserUpdateVo user = new UserUpdateVo();
        user.setUsername(SecurityTestUtils.USER_DEFAULT);
        user.setOldPassword(SecurityTestUtils.PASSWORD_DEFAULT);
        user.setNewPassword(newPassword);

        userService.updateLoggedInUser(user);

        User modifiedUser = userService.findLoggedInUser();
        assertTrue(passwordEncoder.matches(newPassword, modifiedUser.getPassword()));
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void update_userIsNotLoggedIn_throwsSecurityException() {
        userService.updateLoggedInUser(new UserUpdateVo());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void findAll_userIsNotLoggedIn_throwsSecurityException() {
        userService.findAll();
    }

    @Test(expected = AccessDeniedException.class)
    public void findAll_userIsNotAdmin_throwsSecurityException() {
        SecurityTestUtils.loginTestUser();
        userService.findAll();
    }

    @Test
    public void findAll_userIsAdmin_returnUsers() {
        SecurityTestUtils.loginAdminUser();
        List<User> users = userService.findAll();
        assertNotNull(users);
        assertTrue(users.size() > 0);
    }
}